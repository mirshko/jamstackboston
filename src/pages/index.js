import React from 'react';
import Link from 'gatsby-link';

import '../styles/app.scss';

const IndexPage = () => (
  <div>
    <h1 style={{
      fontFamily: 'leitura, serif',
      fontWeight: '400'
    }}>
      <span style={{ color: '#00c7b7' }}>JAMstack</span>: noun \’jam-stak’\<br/>Modern web development architecture based on client-side JavaScript, reusable APIs, and prebuilt Markup.
    </h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great.</p>
    <Link to="/page-2/">Go to page 2</Link>
  </div>
)

export default IndexPage