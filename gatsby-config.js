module.exports = {
  siteMetadata: {
    title: `JAMStack Boston`
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-plugin-postcss-sass`,
      options: {
        postCssPlugins: []
      }
    }
  ]
}